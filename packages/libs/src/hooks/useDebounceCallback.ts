import { useCallback, useRef } from "react";

const useDebounceCallback = (
  callback: (...args: any) => void,
  delay: number
) => {
  const debounceTimer = useRef<number>(0);

  clearTimeout(debounceTimer.current);
  const debounceCallback = useCallback(
    (...args: any) => {
      debounceTimer.current = setTimeout(() => {
        clearTimeout(debounceTimer.current);
        return callback(...args);
      }, delay);
    },
    [callback, delay]
  );

  return debounceCallback;
};

export default useDebounceCallback;
