import { Command } from "commander";
import prompts from "prompts";

export const add = new Command();

add
  .name("add")
  .description("Split a string into substrings and display as an array")
  .argument("[components...]", "string to split")
  .action(async (args, options) => {
    if (!args || !args.length) {
      const response = await prompts({
        type: "multiselect",
        name: "components",
        message: "Pick Components",
        choices: [
          {
            title: "Accordian",
            value: { label: "Accordian", path: "./src/Accordian" },
          },
          { title: "Button", value: { label: "Button", path: "./src/Button" } },
          {
            title: "Carousel",
            value: { label: "Carousel", path: "./src/Carousel" },
          },
        ],
      });
      const notiMessage = `Fetch components ${response.components
        .map(({ label }: { label: string }) => `[${label}]`)
        .join(", ")} from Registry....`;

      console.log(notiMessage);
    }
  });
