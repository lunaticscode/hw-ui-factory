import { Command } from "commander";
import { getPackageJsonData } from "./utils";
import { add } from "./add";

const main = async () => {
  const packageJson = await getPackageJsonData();
  const program = new Command();
  program
    .name("hw-rui-cli")
    .description("add ui-component with dependencies to your project.")
    .version(
      packageJson.version || "1.0.0",
      "-v, --version",
      "display the version."
    );
  program.addCommand(add);
  program.parse();
};
main();
