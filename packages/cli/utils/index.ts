import path from "path";
import fs from "fs/promises";
export const getPackageJsonData = async () => {
  const data = await fs.readFile(path.join("package.json"), "utf-8");
  return JSON.parse(data);
};
