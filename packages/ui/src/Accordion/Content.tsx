import { FC, PropsWithChildren } from "react";
import { useAccordionItemContext } from "./Item";
import { useAccordionContext } from "./Root";
import Primitive from "../Primitve";

export interface AccordionContentProps extends PropsWithChildren {}
const Content: FC<AccordionContentProps> = (props) => {
  const { children } = props;
  const { internalValue: expandedValues } = useAccordionContext();
  const { itemValue } = useAccordionItemContext();
  return itemValue && expandedValues?.includes(itemValue) ? (
    <Primitive.div
      role={"region"}
      id={`${itemValue}-content`}
      aria-labelledby={`${itemValue}-trigger`}
    >
      {children}
    </Primitive.div>
  ) : null;
};
export default Content;
