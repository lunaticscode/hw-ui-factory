import { FC, PropsWithChildren, createContext, useContext } from "react";

import { useControlled } from "@repo/libs";
export type AccordionValue = string[] | undefined;
export interface AccordionRootProps extends PropsWithChildren {
  value?: AccordionValue;
  onValueChange?: (value: AccordionValue) => void;
  defaultValue?: AccordionValue;
  disabled?: boolean;
}

export interface AccordionRootContextProps {
  internalValue?: AccordionValue;
  handleExpandItem?: (value: string, expanded: boolean) => void;
}

export const AccordionContext = createContext<AccordionRootContextProps>({
  internalValue: undefined,
  handleExpandItem: () => {},
});
export const useAccordionContext = () => {
  const context = useContext(AccordionContext);
  if (!context) {
    throw new Error("(!) Accordion 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

const AccoridanRoot: FC<AccordionRootProps> = (props) => {
  const {
    children,
    value: valueProp,
    defaultValue: defaultValueProp,
    onValueChange,
    disabled,
  } = props;

  const { value: internalValue, setValue } = useControlled(
    valueProp,
    defaultValueProp
  );

  const handleExpandItem = (value: string, expanded: boolean) => {
    if (disabled) return;
    const afterValue = expanded
      ? [...(internalValue || []), value]
      : internalValue?.filter((prevValue) => prevValue !== value) || [];
    setValue(afterValue);
    onValueChange?.(afterValue);
  };

  const contextValue: AccordionRootContextProps = {
    internalValue,
    handleExpandItem,
  };
  return (
    <AccordionContext.Provider value={contextValue}>
      {children}
    </AccordionContext.Provider>
  );
};
export default AccoridanRoot;
