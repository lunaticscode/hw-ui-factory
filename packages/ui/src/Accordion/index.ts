//* https://www.w3.org/WAI/ARIA/apg/patterns/accordion/examples/accordion/
import Root from "./Root";
import Item from "./Item";
import Trigger from "./Trigger";
import Header from "./Header";
import Content from "./Content";
import "../styles/common/index.scss";

const Accordion = {
  Root,
  Item,
  Header,
  Trigger,
  Content,
};

export default Accordion;
