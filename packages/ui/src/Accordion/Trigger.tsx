import { HTMLAttributes, PropsWithChildren, forwardRef } from "react";
import { useAccordionItemContext } from "./Item";
import { useAccordionContext } from "./Root";
import Primitive from "../Primitve";

interface AccordianPrimitveTriggerProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLButtonElement> {}
const AccordianPrimitveTrigger = forwardRef<
  HTMLButtonElement,
  AccordianPrimitveTriggerProps
>((props, ref) => {
  const { children, ...triggerProps } = props;
  return (
    <Primitive.button {...triggerProps} ref={ref}>
      {children}
    </Primitive.button>
  );
});

export interface AccordionTriggerProps extends PropsWithChildren {}

const AccordionTrigger = forwardRef<HTMLButtonElement, AccordionTriggerProps>(
  (props, ref) => {
    const { children } = props;

    // const slottableExist = Children.toArray(children).find(isSlottable);
    const { itemValue } = useAccordionItemContext();
    const { handleExpandItem, internalValue: expandedValues } =
      useAccordionContext();
    const handleClickTrigger = () => {
      if (!itemValue) return;
      const isExpanded = expandedValues?.includes(itemValue);
      handleExpandItem?.(itemValue || "", !isExpanded);
    };

    // if (slottableExist) {
    //   return (
    //     <Slot {...props} ref={ref}>
    //       {children}
    //     </Slot>
    //   );
    // }
    // return isValidElement(children) ? (
    //   <Slot {...props} ref={ref}>
    //     {children}
    //   </Slot>
    // ) :
    return (
      <AccordianPrimitveTrigger
        onClick={handleClickTrigger}
        id={`${itemValue}-trigger`}
        aria-controls={`${itemValue}-content`}
        aria-expanded={expandedValues?.includes(itemValue || "") || false}
        ref={ref}
      >
        {children || "trigger"}
      </AccordianPrimitveTrigger>
    );
  }
);
export default AccordionTrigger;
