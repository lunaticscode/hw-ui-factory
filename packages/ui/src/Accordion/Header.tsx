import { PropsWithChildren, forwardRef } from "react";
import Primitive from "../Primitve";

export interface AccordionHeaderProps extends PropsWithChildren {}
const AccordionHeader = forwardRef<HTMLHeadingElement, AccordionHeaderProps>(
  (props, ref) => {
    const { children } = props;
    return <Primitive.h3 ref={ref}>{children}</Primitive.h3>;
  }
);

export default AccordionHeader;
