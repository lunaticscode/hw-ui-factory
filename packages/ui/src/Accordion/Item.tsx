import { FC, PropsWithChildren, createContext, useContext, useId } from "react";
import Primitive from "../Primitve";

export interface AccordionItemContextProps {
  itemValue?: string;
}

const AccordionItemContext = createContext<AccordionItemContextProps>({
  itemValue: undefined,
});

export const useAccordionItemContext = () => {
  const context = useContext(AccordionItemContext);
  if (!context) {
    throw new Error("(!) AccordionItem 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface AccordionItemProps extends PropsWithChildren {
  value?: string;
}
const AccordionItem: FC<AccordionItemProps> = (props) => {
  const { children, value = useId() } = props;
  const contextValue = {
    itemValue: value,
  };
  return (
    <AccordionItemContext.Provider value={contextValue}>
      <Primitive.div>{children}</Primitive.div>
    </AccordionItemContext.Provider>
  );
};
export default AccordionItem;
