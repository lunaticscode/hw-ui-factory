import {
  FC,
  PropsWithChildren /** forwardRef */,
  createContext,
  useContext,
} from "react";
import usePagination from "./hooks/usePagination";

//
interface RootContextProps {
  onClickNav: (direction: number) => void;
  onClickPage: (page: number) => void;
  currentPage: number;
  itemLength: number;
  divider: number;
  pages: number[];
}

const PaginationContext = createContext<RootContextProps>({
  onClickNav: () => {},
  onClickPage: () => {},
  itemLength: 0,
  currentPage: 1,
  divider: 10,
  pages: [],
});

export const usePaginationContext = () => {
  const context = useContext(PaginationContext);
  if (!context) {
    throw new Error("(!) Pagination 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface RootProps extends PropsWithChildren {
  defaultValue?: number;
  itemLength?: number;
  divider?: number;
  onPageChange?: (page: number) => void;
  value?: number;
}

const Root: FC<RootProps> = (props) => {
  const {
    children,
    defaultValue = 1,
    itemLength = 0,
    divider = 10,
    value,
    onPageChange,
  } = props;

  const { currentPage, setCurrentPage, isLastPage, isFirstPage, pages } =
    usePagination({
      itemLength,
      divider,
      defaultValue,
      value,
    });

  const onClickNav = (direction: number) => {
    if (direction === 1 && isLastPage) return;
    if (direction === -1 && isFirstPage) return;
    const resultPage = direction + currentPage;
    onPageChange?.(resultPage);
    setCurrentPage(resultPage);
  };

  const onClickPage = (page: number) => {
    onPageChange?.(page);
    setCurrentPage(page);
  };

  const contextValue = {
    onClickNav,
    onClickPage,
    currentPage,
    itemLength,
    divider,
    pages,
  };

  return (
    <PaginationContext.Provider value={contextValue}>
      {children}
    </PaginationContext.Provider>
  );
};
export default Root;
