import { ReactNode, FC } from "react";
import { usePaginationContext } from "./Root";
import Primitive from "../Primitve";
export interface PageButtonsProps {
  children?: (
    pages: number[],
    currentPage: number,
    handleClickPage: (page: number) => void
  ) => ReactNode;
}

// ForwardRef
const PageButtons: FC<PageButtonsProps> = (props) => {
  const { children } = props;
  const { currentPage, onClickPage, pages } = usePaginationContext();

  return (
    <>
      {!children &&
        pages.map((page) => (
          <Primitive.button
            key={`pagination-button-${page}`}
            style={{ color: page === currentPage ? "red" : "black" }}
            onClick={() => onClickPage(page)}
          >
            {page}
          </Primitive.button>
        ))}
      {children && children(pages, currentPage, onClickPage)}
    </>
  );
};
export default PageButtons;
