import { FC, ReactNode } from "react";
import { usePaginationContext } from "./Root";
import Primitive from "../Primitve";

export interface NavigatorProps {
  children?: (
    handleClickPrev: () => void,
    handleClickNext: () => void
  ) => ReactNode;
}

// ForwardRef
const Navigator: FC<NavigatorProps> = ({ children }) => {
  const { onClickNav } = usePaginationContext();
  const handleClickPrev = () => {
    onClickNav(-1);
  };

  const handleClickNext = () => {
    onClickNav(1);
  };
  return (
    <>
      {!children && (
        <>
          <Primitive.button onClick={handleClickPrev}>prev</Primitive.button>
          <Primitive.button onClick={handleClickNext}>next</Primitive.button>
        </>
      )}
      {children && children(handleClickPrev, handleClickNext)}
    </>
  );
};
export default Navigator;
