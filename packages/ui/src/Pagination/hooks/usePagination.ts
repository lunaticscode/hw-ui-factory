import { useControlled } from "@repo/libs";
import { useMemo } from "react";
type UsePaginationArgs = {
  defaultValue?: number;
  itemLength?: number;
  divider?: number;
  value?: number;
};

const getMemorizedRule = (currentPage: number, divider: number) => {
  return (Math.ceil(currentPage / divider) - 1) * divider;
};

const usePagination = ({
  itemLength = 0,
  divider = 10,
  defaultValue,
  value,
}: UsePaginationArgs) => {
  const { value: currentPage = 1, setValue: setCurrentPage } = useControlled(
    value,
    defaultValue
  );

  const isFirstPage = useMemo(() => currentPage === 1, [currentPage]);

  const isLastPage = useMemo(
    () => Math.ceil(itemLength / divider) === currentPage,
    [itemLength, divider, currentPage]
  );

  const pagesLength = useMemo(
    () =>
      Math.ceil(currentPage / divider) * divider <
      Math.ceil(itemLength / divider)
        ? divider
        : Math.ceil(itemLength / divider) % divider,
    [currentPage, divider, itemLength]
  );

  const pages = useMemo(
    () =>
      Array.from(
        { length: pagesLength },
        (_, index) =>
          (Math.ceil(currentPage / divider) - 1) * divider + (index + 1)
      ),
    // eslint-disable-next-line react-hooks/exhaustive-deps
    [getMemorizedRule(currentPage, divider), pagesLength]
  );

  return {
    currentPage,
    setCurrentPage,
    isFirstPage,
    isLastPage,
    pages,
  };
};
export default usePagination;
