import Root from "./Root";
import Navigator from "./Navigator";
import PageButtons from "./PageButtons";

const Paginator = {
  Root,
  Navigator,
  PageButtons,
};

export default Paginator;
