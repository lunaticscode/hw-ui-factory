import { FC, PropsWithChildren, RefObject } from "react";
import { usePopoverContext } from "./Root";
import usePopoverPortal from "./hooks/usePopoverPortal";
export type PopoverPositions =
  | "top-left"
  | "top-right"
  | "bottom-left"
  | "bottom-right";
export interface PopoverPortalProps extends PropsWithChildren {
  anchorRef?: RefObject<HTMLElement>;
  containerRef?: RefObject<HTMLElement>;
  position?: PopoverPositions;
}
const PopoverPortal: FC<PopoverPortalProps> = ({ children, ...props }) => {
  const { anchorRef, containerRef, position } = props;
  const { open } = usePopoverContext();
  const { Portal } = usePopoverPortal(anchorRef, containerRef, position);
  return open ? <Portal>{children}</Portal> : null;
};
export default PopoverPortal;
