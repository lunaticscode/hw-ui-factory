import {
  HTMLAttributes,
  PropsWithChildren,
  ReactElement,
  ReactNode,
  cloneElement,
  forwardRef,
} from "react";
import { usePopoverContext } from "./Root";
import Primitive from "../Primitve";
interface PopoverPrimitiveCloseProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLButtonElement> {}

const PopoverPrimitiveClose = forwardRef<
  HTMLButtonElement,
  PopoverPrimitiveCloseProps
>((props, ref) => {
  const { children, onClick } = props;
  return (
    <Primitive.button ref={ref} onClick={onClick}>
      {children}
    </Primitive.button>
  );
});

export interface PopoverCloseProps {
  children?: (handleClose?: () => void) => ReactNode | ReactNode;
}

const PopoverClose = forwardRef<HTMLButtonElement, PopoverCloseProps>(
  (props, ref) => {
    const { children } = props;
    const { handleClose } = usePopoverContext();

    if (children && typeof children === "function") {
      const newElement = children(handleClose) as ReactElement;
      return cloneElement(newElement, { ...newElement.props, ref });
    }

    return (
      <PopoverPrimitiveClose ref={ref} onClick={handleClose}>
        {children ?? "popover-close"}
      </PopoverPrimitiveClose>
    );
  }
);
export default PopoverClose;
