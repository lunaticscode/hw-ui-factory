import {
  CSSProperties,
  FC,
  ReactNode,
  RefObject,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import { createPortal } from "react-dom";
import { PopoverPositions } from "../Portal";
import Primitive from "../../Primitve";

const getPopoverPostionValue = (
  tx: number,
  ty: number,
  tw: number,
  th: number,
  position: PopoverPositions
) => {
  const mapPostionTypeToPositionValue: Record<
    PopoverPositions,
    { x: number; y: number }
  > = {
    "bottom-left": { x: tx, y: ty + th },
    "bottom-right": { x: tw - tx, y: ty + th },
    "top-left": { x: tx, y: ty - th },
    "top-right": { x: tw - tx, y: ty - th },
  };
  return mapPostionTypeToPositionValue[position];
};

const usePopoverPortal = (
  anchorRef?: RefObject<HTMLElement>,
  containerRef?: RefObject<HTMLElement>,
  position: PopoverPositions = "bottom-left"
) => {
  const [containerElement, setContainerElement] = useState<Element>();
  useEffect(() => {
    if (containerRef && containerRef.current) {
      setContainerElement(containerRef.current);
    } else {
      setContainerElement(document.body);
    }
  }, [anchorRef]);

  const anchorRect = useMemo(
    () => anchorRef?.current?.getBoundingClientRect(),
    [anchorRef?.current]
  );
  const [tx, ty, tw, th] = useMemo(
    () => [
      anchorRect?.x ?? 0,
      anchorRect?.y ?? 0,
      anchorRect?.width ?? 0,
      anchorRect?.height ?? 0,
    ],
    [anchorRect]
  );

  const { x, y } = useMemo(
    () => getPopoverPostionValue(tx, ty, tw, th, position),
    [tx, ty, tw, th, position]
  );

  const Portal: FC<{ children?: ReactNode }> = useCallback(
    ({ children }) => {
      return containerElement
        ? createPortal(
            <Primitive.div
              style={{
                position: "absolute",
                top: `${y}px`,
                left: `${x}px`,
              }}
            >
              {children}
            </Primitive.div>,
            containerElement
          )
        : null;
    },
    [containerElement]
  );

  return { Portal };
};

export default usePopoverPortal;
