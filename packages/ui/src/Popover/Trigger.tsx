import {
  HTMLAttributes,
  PropsWithChildren,
  ReactElement,
  ReactNode,
  cloneElement,
  forwardRef,
  useEffect,
  useRef,
} from "react";
import { usePopoverContext } from "./Root";
import { useMergedRef } from "@repo/libs";
import Primitive from "../Primitve";

export interface PopoverPrimitiveTriggerProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLButtonElement> {}

const PopoverPrimitiveTrigger = forwardRef<
  HTMLButtonElement,
  PopoverPrimitiveTriggerProps
>((props, ref) => {
  const { children, onClick } = props;
  return (
    <Primitive.button ref={ref} onClick={onClick}>
      {children ?? "open"}
    </Primitive.button>
  );
});

export interface PopoverTriggerProps {
  children?: ((handleClick: () => void) => ReactNode) | ReactNode;
}

const Trigger = forwardRef<HTMLButtonElement, PopoverTriggerProps>(
  (props, ref) => {
    const { children } = props;
    const triggerRef = useRef<HTMLButtonElement>(null);
    const handleMergeRef = useMergedRef(triggerRef, ref);
    const { open, handleOpen, handleClose, setTriggerRef } =
      usePopoverContext();
    const handleClick = () => {
      if (open) {
        handleClose?.();
      } else {
        handleOpen?.();
      }
    };

    useEffect(() => {
      if (ref && ref) {
        setTriggerRef(triggerRef);
      }
    }, [ref]);

    if (children && typeof children === "function") {
      const newElement = children(handleClick) as ReactElement;
      return cloneElement(newElement, {
        ...newElement.props,
        ref: handleMergeRef,
      });
    }

    return (
      <PopoverPrimitiveTrigger ref={handleMergeRef} onClick={handleClick}>
        {children ?? "popover-trigger"}
      </PopoverPrimitiveTrigger>
    );
  }
);
export default Trigger;
