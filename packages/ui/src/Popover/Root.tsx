import { useControlled } from "@repo/libs";
import {
  Dispatch,
  FC,
  PropsWithChildren,
  RefObject,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from "react";

export interface PopoverProps extends PropsWithChildren {
  open?: boolean;
  defaultOpen?: boolean;
  onOpenChange?: (open: boolean) => void;
}

export interface PopoverContextProps extends Pick<PopoverProps, "open"> {
  handleOpen?: () => void;
  handleClose?: () => void;
  triggerRef: RefObject<HTMLButtonElement>;
  setTriggerRef: Dispatch<SetStateAction<RefObject<HTMLButtonElement>>>;
}

const PopoverContext = createContext<PopoverContextProps>({
  handleOpen: () => {},
  handleClose: () => {},
  triggerRef: { current: null },
  setTriggerRef: () => {},
});

export const usePopoverContext = () => {
  const context = useContext(PopoverContext);
  if (!context) {
    throw new Error("(!) Popover 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

const PopoverRoot: FC<PopoverProps> = (props) => {
  const {
    children,
    open: openProp,
    defaultOpen: defaultOpenProp,
    onOpenChange,
  } = props;
  const [triggerRef, setTriggerRef] = useState<RefObject<HTMLButtonElement>>({
    current: null,
  });
  const { value: open, setValue: setOpen } = useControlled(
    openProp,
    defaultOpenProp
  );

  const handleOpen = () => {
    onOpenChange?.(true);
    setOpen(true);
  };
  const handleClose = () => {
    onOpenChange?.(false);
    setOpen(false);
  };
  const contextValue: PopoverContextProps = {
    open,
    handleOpen,
    setTriggerRef,
    triggerRef,
    handleClose,
  };
  return (
    <PopoverContext.Provider value={contextValue}>
      {children}
    </PopoverContext.Provider>
  );
};
export default PopoverRoot;
