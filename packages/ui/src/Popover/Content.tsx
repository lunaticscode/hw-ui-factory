import { HTMLAttributes, PropsWithChildren, forwardRef } from "react";
import Primitive from "../Primitve";

export interface PopoverContentProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLDivElement> {}

const PopoverContent = forwardRef<HTMLDivElement, PopoverContentProps>(
  (props, ref) => {
    const { children } = props;
    return <Primitive.div ref={ref}>{children}</Primitive.div>;
  }
);

export default PopoverContent;
