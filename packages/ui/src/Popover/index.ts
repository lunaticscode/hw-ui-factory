import Root from "./Root";
import Trigger from "./Trigger";
import Portal from "./Portal";
import Content from "./Content";
import Close from "./Close";

const Popover = {
  Root,
  Trigger,
  Portal,
  Content,
  Close,
};

export default Popover;
