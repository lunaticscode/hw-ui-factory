import { ReactNode, forwardRef } from "react";
import { useCarouselContext } from "./Root";
import Primitive from "../Primitve";

interface CarouselPrimitiveTriggerProps {
  onClickPrev: () => void;
  onClickNext: () => void;
  carouselId: string;
}
const CarouselPrimitiveTrigger = forwardRef<
  HTMLDivElement,
  CarouselPrimitiveTriggerProps
>((props, ref) => {
  const { onClickPrev, onClickNext, carouselId } = props;
  return (
    <Primitive.div ref={ref}>
      <Primitive.button
        aria-controls={carouselId}
        aria-label="Previous Slide"
        onClick={onClickPrev}
      >
        prev
      </Primitive.button>
      <Primitive.button
        aria-controls={carouselId}
        aria-label="Next Slide"
        onClick={onClickNext}
      >
        next
      </Primitive.button>
    </Primitive.div>
  );
});

export interface CarouselTriggerProps {
  children?: (prevTrigger: () => void, nextTrigger: () => void) => ReactNode;
}
const CarouselTrigger = forwardRef<HTMLDivElement, CarouselTriggerProps>(
  (props, ref) => {
    const { children } = props;
    const { currentIndex, handleChangeIndex, carouselId } =
      useCarouselContext();
    const handleClickNext = () => {
      handleChangeIndex(currentIndex + 1);
    };
    const handleClickPrev = () => {
      handleChangeIndex(currentIndex - 1);
    };
    return (
      <div ref={ref}>
        {children ? (
          children(handleClickPrev, handleClickNext)
        ) : (
          <CarouselPrimitiveTrigger
            ref={ref}
            onClickPrev={() => handleChangeIndex(currentIndex - 1)}
            onClickNext={() => handleChangeIndex(currentIndex + 1)}
            carouselId={carouselId}
          />
        )}
      </div>
    );
  }
);
export default CarouselTrigger;
