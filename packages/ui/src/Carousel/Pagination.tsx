import { FC, ReactNode, useMemo } from "react";
import { useCarouselContext } from "./Root";
import Primitive from "../Primitve";

export interface CarouselPaginationProps {
  children?: (slideIndexs: number[]) => ReactNode;
}
const CarouselPagination: FC<CarouselPaginationProps> = (props) => {
  const { children } = props;
  const { slideLength } = useCarouselContext();
  const slideIndexs = useMemo(
    () => Array.from({ length: slideLength }, (_, index) => index + 1),
    [slideLength]
  );
  return (
    <Primitive.div>
      {children ? children(slideIndexs) : <div>{slideIndexs}</div>}
    </Primitive.div>
  );
};

export default CarouselPagination;
