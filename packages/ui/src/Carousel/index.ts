// https://www.w3.org/WAI/ARIA/apg/patterns/carousel/examples/carousel-1-prev-next/
import Root from "./Root";
import Slide from "./Slide";
import Slider from "./Slider";
import Trigger from "./Trigger";
import Pagination from "./Pagination";

import "../styles/common/index.scss";

const Carousel = {
  Root,
  Trigger,
  Slider,
  Slide,
  Pagination,
};

export default Carousel;
