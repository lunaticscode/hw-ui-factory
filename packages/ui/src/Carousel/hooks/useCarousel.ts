import { Children, ReactElement, ReactNode, useMemo, useState } from "react";
import CarouselSlider from "../Slider";

const useCarousel = (
  children: ReactNode,
  onValueChange?: (index: number) => void
) => {
  const [currentIndex, setCurrentIndex] = useState<number>(0);
  const carouselSlider = useMemo(
    () =>
      (Children.toArray(children) as ReactElement[]).find(
        (child) => child.type === CarouselSlider
      ),
    [children]
  );

  const slideLength = useMemo(
    () => Children.count(carouselSlider?.props.children) ?? 0,
    [carouselSlider]
  );

  const handleChangeIndex = (index: number) => {
    console.log("handleChangeIndex", { index });
    let updatedIndex = index;
    if (index >= slideLength) {
      updatedIndex = 0;
    }
    if (index < 0) {
      updatedIndex = slideLength - 1;
    }
    setCurrentIndex(updatedIndex);
    onValueChange?.(updatedIndex);
  };

  return {
    currentIndex,
    handleChangeIndex,
    slideLength,
  };
};
export default useCarousel;
