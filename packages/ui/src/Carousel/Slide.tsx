import { FC, PropsWithChildren } from "react";
import { useCarouselContext } from "./Root";
import Primitive from "../Primitve";

export interface CarouselSlideProps extends PropsWithChildren {
  value?: string;
  index?: number;
}
const CarouselSlide: FC<CarouselSlideProps> = ({ children, ...props }) => {
  const { index } = props;

  const { currentIndex, slideLength } = useCarouselContext();

  return currentIndex === index ? (
    <Primitive.li
      role="group"
      aria-label={`${index + 1} of ${slideLength}`}
      aria-roledescription="slide"
    >
      {children}
    </Primitive.li>
  ) : null;
};
export default CarouselSlide;
