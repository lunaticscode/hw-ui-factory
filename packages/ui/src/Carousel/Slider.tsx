import {
  Children,
  FC,
  PropsWithChildren,
  ReactElement,
  cloneElement,
} from "react";
import CarouselSlide from "./Slide";
import Primitive from "../Primitve";

export interface CarouselSliderProps extends PropsWithChildren {}
const CarouselSlider: FC<CarouselSliderProps> = (props) => {
  const { children } = props;
  const _children = Children.toArray(children) as ReactElement[];
  const carouselSlides = _children.filter(
    (child) => child.type === CarouselSlide
  );

  return (
    <Primitive.ul>
      {carouselSlides.map((slide, index) =>
        cloneElement(slide, { ...slide.props, index })
      )}
    </Primitive.ul>
  );
};
export default CarouselSlider;
