import {
  PropsWithChildren,
  createContext,
  forwardRef,
  useContext,
  useId,
  useImperativeHandle,
} from "react";
import { useCarousel } from "./hooks";

export interface CarouselProps extends PropsWithChildren {
  onValueChange?: (value?: CarouselValue) => void;
}

export type CarouselValue = number;
interface CarouselContextProps extends PropsWithChildren {
  currentIndex: number;
  handleChangeIndex: (index: number) => void;
  slideLength: number;
  carouselId: string;
}

const CarouselContext = createContext<CarouselContextProps>({
  currentIndex: 0,
  handleChangeIndex: () => {},
  slideLength: 0,
  carouselId: "",
});

export const useCarouselContext = () => {
  const context = useContext(CarouselContext);
  if (!context) {
    throw new Error("(!) Carousel 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface CarouselRef {
  goTo?: (index: number) => void;
  prevSlide?: () => void;
  nextSlide?: () => void;
}

const CarouselRoot = forwardRef<CarouselRef, CarouselProps>((props, ref) => {
  const carouselId = useId();
  const { children, onValueChange } = props;
  const { currentIndex, slideLength, handleChangeIndex } = useCarousel(
    children,
    onValueChange
  );

  useImperativeHandle(ref, () => ({
    goTo: (index) => handleChangeIndex(index),
    prevSlide: () => handleChangeIndex(currentIndex - 1),
    nextSlide: () => handleChangeIndex(currentIndex + 1),
  }));

  const contextValue = {
    currentIndex,
    handleChangeIndex,
    slideLength,
    carouselId,
  };

  return (
    <CarouselContext.Provider value={contextValue}>
      <section role="region" aria-roledescription="carousel">
        {children}
      </section>
    </CarouselContext.Provider>
  );
});
export default CarouselRoot;
