import { FC, PropsWithChildren } from "react";
import Primitive from "../Primitve";

interface CarouselIndicatorProps extends PropsWithChildren {}
const CarouselIndicator: FC<CarouselIndicatorProps> = (props) => {
  const { children } = props;
  return <Primitive.div>{children}</Primitive.div>;
};
export default CarouselIndicator;

/**
 *
 *  <Indicator>
 *    <Trigger />
 *    <Pagination />
 *  </Indicator>
 */
