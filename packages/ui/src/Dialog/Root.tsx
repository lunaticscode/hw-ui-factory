import { useControlled } from "@repo/libs";
import { FC, PropsWithChildren, createContext, useContext } from "react";

export interface DialogRootProps extends PropsWithChildren {
  open?: boolean;
  defaultOpen?: boolean;
  onOpenChange?: (open: boolean) => void;
}
interface DialogContextProps extends Pick<DialogRootProps, "open"> {
  handleClickTrigger?: () => void;
}
const DialogContext = createContext<DialogContextProps>({
  open: false,
  handleClickTrigger: () => {},
});

export const useDialogContext = () => {
  const context = useContext(DialogContext);
  if (!context) {
    throw Error("(!) Dialog 컨텍스트를 호출할 수 없는 범위 입니다.");
  }
  return context;
};
const DialogRoot: FC<DialogRootProps> = (props) => {
  const { children, open: openProp, defaultOpen: defaultOpenProp } = props;

  const { value: open, setValue: setOpen } = useControlled(
    openProp,
    defaultOpenProp
  );

  const handleClickTrigger = () => {
    setOpen(true);
  };

  const contextValue: DialogContextProps = {
    open,
    handleClickTrigger,
  };
  return (
    <DialogContext.Provider value={contextValue}>
      {children}
    </DialogContext.Provider>
  );
};
export default DialogRoot;
