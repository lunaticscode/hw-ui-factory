import {
  Children,
  FC,
  PropsWithChildren,
  ReactElement,
  cloneElement,
  forwardRef,
  isValidElement,
} from "react";
import Primitive from "../Primitve";
import { useDialogContext } from "./Root";

const DEFAULT_DIALOG_TRIGGER_LABEL = "trigger";

export interface DialogTriggerProps extends PropsWithChildren {}
const DialogTrigger = forwardRef<HTMLElement, DialogTriggerProps>(
  (props, ref) => {
    const { children } = props;
    const { handleClickTrigger } = useDialogContext();

    const child = (
      isValidElement(children) ? (
        Children.only(children)
      ) : (
        <Primitive.button>
          {children ?? DEFAULT_DIALOG_TRIGGER_LABEL}
        </Primitive.button>
      )
    ) as ReactElement;

    return cloneElement(child, {
      ...child.props,
      ref,
      onClick: () => {
        handleClickTrigger?.();
        child.props.onClick?.();
      },
    });
  }
);
export default DialogTrigger;
