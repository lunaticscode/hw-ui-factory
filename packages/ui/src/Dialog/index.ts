import Root from "./Root";
import Trigger from "./Trigger";
import Content from "./Content";

const Dialog = {
  Root,
  Trigger,
  Content,
};

export default Dialog;
