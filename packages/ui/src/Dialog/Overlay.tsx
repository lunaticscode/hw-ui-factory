import { FC } from "react";
import { useDialogContext } from "./Root";
import Primitive from "../Primitve";

interface DialogOverlayProps {}
const Overlay: FC<DialogOverlayProps> = () => {
  const { open: openState } = useDialogContext();

  return openState ? <Primitive.div /> : null;
};
export default Overlay;
