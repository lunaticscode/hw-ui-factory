import { FC, PropsWithChildren } from "react";

export interface DialogContent extends PropsWithChildren {}
const DialogContent: FC<DialogContent> = (props) => {
  const { children } = props;
  return <>{children}</>;
};
export default DialogContent;
