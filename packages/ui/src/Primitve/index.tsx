import { forwardRef } from "react";
import { useTrackingContext } from "../Tracking/Provider";

const ELEMENT_TAGS = [
  "h3",
  "div",
  "button",
  "input",
  "a",
  "form",
  "nav",
  "ul",
  "li",
] as const;

type PrimitiveElementType = {
  [E in (typeof ELEMENT_TAGS)[number]]: React.ForwardRefExoticComponent<
    React.ComponentPropsWithRef<E>
  >;
};

const Primitive = ELEMENT_TAGS.reduce((primitives, node) => {
  const element = forwardRef((props, ref: any) => {
    const Component = node;
    // const {} = useTrackingContext();
    return (
      <>
        <Component {...props} ref={ref} />
      </>
    );
  });
  // element.displayName = `Primitive.${node}`;
  return { ...primitives, [node]: element };
}, {} as PrimitiveElementType);

export default Primitive;
