import { useControlled } from "@repo/libs";
import { FC, PropsWithChildren, createContext, useContext } from "react";

export interface RootProps extends PropsWithChildren {
  value?: string;
  defaultValue?: string;
  onValueChange?: (value: string) => void;
  disabled?: boolean | string[];
}

interface TabsContextProps extends Pick<RootProps, "value"> {
  handleClickTab?: (value?: string) => void;
}
const TabsContext = createContext<TabsContextProps>({
  value: undefined,
  handleClickTab: () => {},
});

export const useTabsContext = () => {
  const context = useContext(TabsContext);
  if (!context) {
    throw Error("(!) Tabs 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

const Root: FC<RootProps> = (props) => {
  const {
    children,
    value: valueProp,
    defaultValue: defaultValueProp,
    onValueChange,
    disabled,
  } = props;
  const { value, setValue } = useControlled(valueProp, defaultValueProp);

  const handleClickTab = (value?: string) => {
    if (!value) return;
    if (
      disabled === true ||
      (Array.isArray(disabled) && disabled.includes(value))
    ) {
      return;
    }
    setValue(value);
    onValueChange?.(value);
  };
  const contextValue: TabsContextProps = {
    value,
    handleClickTab,
  };

  return (
    <TabsContext.Provider value={contextValue}>{children}</TabsContext.Provider>
  );
};
export default Root;
