import { PropsWithChildren, forwardRef, useCallback } from "react";
import Primitive from "../Primitve";
import { useTabsContext } from "./Root";
export interface TabsTabProps extends PropsWithChildren {
  value?: string;
  disabled?: boolean;
}
const Tab = forwardRef<HTMLButtonElement, TabsTabProps>((props, ref) => {
  const { children, value: valueProp, disabled } = props;

  const { value, handleClickTab } = useTabsContext();

  const onClickTab = useCallback(() => {
    if (value === valueProp || disabled) return;
    handleClickTab?.(valueProp);
  }, [value, valueProp, disabled]);

  return (
    <Primitive.button role={"tab"} onClick={onClickTab} ref={ref}>
      {children}
    </Primitive.button>
  );
});
export default Tab;
