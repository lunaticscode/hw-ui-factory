import Root from "./Root";
import TabList from "./TabList";
import Tab from "./Tab";
import Content from "./Content";
const Tabs = {
  Root,
  TabList,
  Tab,
  Content,
};
export default Tabs;
