import { FC, PropsWithChildren } from "react";
import Primitive from "../Primitve";

export interface TabsTabListProps extends PropsWithChildren {}
const TabList: FC<TabsTabListProps> = (props) => {
  const { children } = props;
  return <Primitive.div role={"tablist"}>{children}</Primitive.div>;
};
export default TabList;
