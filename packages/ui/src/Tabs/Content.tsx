import { FC, PropsWithChildren } from "react";
import Primitive from "../Primitve";
import { useTabsContext } from "./Root";

export interface TabsContentProps extends PropsWithChildren {
  value?: string;
}
const Content: FC<TabsContentProps> = (props) => {
  const { children, value: valueProp } = props;
  const { value } = useTabsContext();

  return value === valueProp ? <Primitive.div>{children}</Primitive.div> : null;
};
export default Content;
