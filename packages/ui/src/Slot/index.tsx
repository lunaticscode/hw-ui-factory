import {
  CSSProperties,
  Children,
  FC,
  HTMLAttributes,
  PropsWithChildren,
  ReactElement,
  ReactNode,
  cloneElement,
  forwardRef,
  isValidElement,
} from "react";

type AnyProps = HTMLAttributes<HTMLElement> & Record<string, any>;
const getMergedProps = (srcProps: AnyProps, targetProps: AnyProps) => {
  const overridedProps = { ...targetProps };

  for (const propName in targetProps) {
    const srcPropValue = srcProps[propName];
    const targetPropValue = targetProps[propName];

    if (srcPropValue && targetPropValue) {
      //* TODO ::: slot과 target의 이벤트를 둘다 실행할 수 있게.
    }
    if (propName === "style") {
      overridedProps[propName] = getMergedStyleProps(
        srcPropValue,
        targetPropValue
      );
    }
    if (propName === "className") {
      overridedProps[propName] = getMergedClassName(
        srcPropValue,
        targetPropValue
      );
    }
  }
  return { ...srcProps, ...overridedProps };
};

const getMergedStyleProps = (
  srcStyleProps: CSSProperties,
  targetStyleProps: CSSProperties
) => {
  return { ...srcStyleProps, targetStyleProps };
};

const getMergedClassName = (srcCls: string, targetCls: string) => {
  return [srcCls, targetCls].filter(Boolean).join(" ");
};

interface SlottableProps extends PropsWithChildren {}
export const Slottable: FC<SlottableProps> = ({ children }) => {
  return <>{children}</>;
};

export const isSlottable = (child: ReactNode) => {
  return isValidElement(child) && child.type === Slottable;
};

interface SlotProps extends HTMLAttributes<HTMLElement>, PropsWithChildren {}
const Slot = forwardRef<HTMLElement, SlotProps>((props, ref) => {
  const { children, ...slotProps } = props;
  const _children = Children.toArray(children) as ReactElement[];
  const slottable = _children.find(isSlottable) as
    | ReactElement<SlottableProps>
    | undefined;

  if (slottable) {
    const newElement = slottable.props.children;
    const newChildren = _children.map((child) => {
      if (child === slottable) {
        return isValidElement(newElement) ? newElement.props.children : null;
      } else {
        return child;
      }
    });

    return isValidElement(newElement)
      ? cloneElement(
          newElement,
          getMergedProps(slotProps, newElement.props),
          newChildren
        )
      : null;
  }

  if (isValidElement(children)) {
    return cloneElement(children, {
      ...getMergedProps(slotProps, children.props),
    });
  }
  return null;
});

export default Slot;
