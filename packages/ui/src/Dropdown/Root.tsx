import {
  Dispatch,
  FC,
  RefObject,
  createContext,
  useContext,
  useRef,
  useState,
} from "react";
import { PopoverContextProps, PopoverProps } from "../Popover/Root";
import { Popover } from "..";
import Primitive from "../Primitve";

export interface DropdownProps extends PopoverProps {}

export interface DropdownContextProps
  extends Pick<PopoverContextProps, "handleClose" | "handleOpen"> {
  anchorRef: RefObject<HTMLElement>;
  setAnchorRef: Dispatch<React.SetStateAction<RefObject<HTMLElement>>>;
  containerRef: RefObject<HTMLElement>;
}

const DropdownContext = createContext<DropdownContextProps>({
  handleClose: () => {},
  handleOpen: () => {},
  anchorRef: { current: null },
  setAnchorRef: () => {},
  containerRef: { current: null },
});

export const useDropdownContext = () => {
  const context = useContext(DropdownContext);
  if (!context) {
    throw new Error("(!) Dropdown 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

const DropdownRoot: FC<DropdownProps> = (props) => {
  const [anchorRef, setAnchorRef] = useState<RefObject<HTMLElement>>({
    current: null,
  });

  const handleClose = () => {};
  const handleOpen = () => {};
  const containerRef = useRef<HTMLDivElement>(null);
  const contextValue: DropdownContextProps = {
    handleClose,
    handleOpen,
    anchorRef,
    setAnchorRef,
    containerRef,
  };

  return (
    <DropdownContext.Provider value={contextValue}>
      <Popover.Root {...props} />
      <Primitive.div ref={containerRef} />
    </DropdownContext.Provider>
  );
};

export default DropdownRoot;
