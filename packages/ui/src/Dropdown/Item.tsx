import { HTMLAttributes, PropsWithChildren, forwardRef } from "react";
import Primitive from "../Primitve";
export interface DropdownItemProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLDivElement> {}

const DropdownItem = forwardRef<HTMLDivElement, DropdownItemProps>(
  (props, ref) => {
    const { children } = props;
    return (
      <Primitive.div ref={ref} role={"menuitem"}>
        {children}
      </Primitive.div>
    );
  }
);

export default DropdownItem;
