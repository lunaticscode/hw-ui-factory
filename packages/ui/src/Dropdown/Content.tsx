import {
  PropsWithChildren,
  createContext,
  forwardRef,
  useContext,
} from "react";

import { useDropdownContext } from "./Root";
import { Popover } from "..";
import Primitive from "../Primitve";

export interface DropdownContentProps extends PropsWithChildren {}
interface DropdownContentContextProps {}
const DropdownContentContext = createContext<DropdownContentContextProps>({});
export const useDropdownContentContext = () => {
  const context = useContext(DropdownContentContext);
  if (!context) {
    throw new Error("(!) Dropdown 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};
const DropdownContent = forwardRef<HTMLDivElement, DropdownContentProps>(
  (props, ref) => {
    const { anchorRef, containerRef } = useDropdownContext();
    const { children } = props;
    return (
      <Popover.Portal anchorRef={anchorRef} containerRef={containerRef}>
        <Primitive.div ref={ref} role={"menu"} {...props}>
          {children}
        </Primitive.div>
      </Popover.Portal>
    );
  }
);
export default DropdownContent;
