import { forwardRef, useEffect, useRef } from "react";
import { PopoverTriggerProps } from "../Popover/Trigger";
import { Popover } from "..";
import { useMergedRef } from "@repo/libs";
import { useDropdownContext } from "./Root";

export interface DropdownTriggerProps extends PopoverTriggerProps {}
const DropdownTrigger = forwardRef<HTMLButtonElement, DropdownTriggerProps>(
  (props, ref) => {
    const triggerRef = useRef<HTMLButtonElement>(null);
    const { setAnchorRef } = useDropdownContext();
    const handleMergeRef = useMergedRef(triggerRef, ref);

    useEffect(() => {
      if (triggerRef.current) {
        setAnchorRef(triggerRef);
      }
    }, []);
    return <Popover.Trigger ref={handleMergeRef} {...props} />;
  }
);
export default DropdownTrigger;
