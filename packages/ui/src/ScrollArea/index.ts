import Root from "./ScrollAreaRoot";
import Viewport from "./ScrollAreaViewport";
import ScrollBar from "./ScrollAreaScrollBar";
import Thumb from "./ScrollAreaThumb";

const ScrollArea = {
  Root,
  Viewport,
  ScrollBar,
  Thumb,
};
export default ScrollArea;
