import {
  CSSProperties,
  FC,
  HTMLAttributes,
  PropsWithChildren,
  WheelEventHandler,
  useEffect,
  useRef,
} from "react";
import Primitive from "../Primitve";
import { useScrollAreaContext } from "./ScrollAreaRoot";

export interface ScrollAreaViewportProps
  extends PropsWithChildren,
    HTMLAttributes<HTMLDivElement> {
  width?: CSSProperties["width"];
  height?: CSSProperties["height"];
}

const ScrollAreaViewport: FC<ScrollAreaViewportProps> = (props) => {
  const viewportRef = useRef<HTMLDivElement>(null);
  const { setViewportRef, setViewportScrollTop } = useScrollAreaContext();

  useEffect(() => {
    if (viewportRef.current) {
      setViewportRef(viewportRef);
    }
  }, [viewportRef.current]);
  const { children, width = "auto", height = "200px", ...restProps } = props;
  const handleWheel: WheelEventHandler<HTMLDivElement> = (e) => {
    setViewportScrollTop(e.currentTarget.scrollTop);
  };

  return (
    <Primitive.div
      style={{
        width,
        height,
        overflow: "auto",
        whiteSpace: "nowrap",
        scrollbarWidth: "none",
      }}
      onWheel={handleWheel}
      ref={viewportRef}
      {...restProps}
    >
      {children}
    </Primitive.div>
  );
};
export default ScrollAreaViewport;
