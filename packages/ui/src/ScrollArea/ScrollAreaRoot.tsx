import {
  Dispatch,
  FC,
  PropsWithChildren,
  RefObject,
  SetStateAction,
  createContext,
  useContext,
  useMemo,
  useState,
} from "react";

export interface ScrollAreaRootProps extends PropsWithChildren {}
interface ScrollAreaContextProps {
  viewportRef: RefObject<HTMLDivElement>;
  setViewportRef: Dispatch<SetStateAction<RefObject<HTMLDivElement>>>;
  viewportScrollTop: number;
  setViewportScrollTop: Dispatch<SetStateAction<number>>;
  scrollHide: boolean;
  setScrollHide: Dispatch<SetStateAction<boolean>>;
}
const ScrollAreaContext = createContext<ScrollAreaContextProps>({
  viewportRef: { current: null },
  setViewportRef: () => {},
  viewportScrollTop: 0,
  setViewportScrollTop: () => {},
  scrollHide: true,
  setScrollHide: () => {},
});

export const useScrollAreaContext = () => {
  const context = useContext(ScrollAreaContext);
  if (!context) {
    throw Error("(!) ScrollArea 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};
const ScrollAreaRoot: FC<ScrollAreaRootProps> = ({ children }) => {
  const [viewportRef, setViewportRef] = useState<RefObject<HTMLDivElement>>({
    current: null,
  });
  const [viewportScrollTop, setViewportScrollTop] = useState<number>(0);
  const [scrollHide, setScrollHide] = useState<boolean>(true);
  const contextValue = useMemo(
    () => ({
      viewportRef,
      setViewportRef,
      viewportScrollTop,
      setViewportScrollTop,
      scrollHide,
      setScrollHide,
    }),
    [viewportRef.current, viewportScrollTop, scrollHide]
  );
  return (
    <ScrollAreaContext.Provider value={contextValue}>
      {children}
    </ScrollAreaContext.Provider>
  );
};
export default ScrollAreaRoot;
