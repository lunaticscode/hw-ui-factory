import { FC } from "react";
import Primitive from "../../Primitve";
import { useCalendarContext } from "../Root";

interface CalendarDateCellProps {
  date: Date;
}
const DateCell: FC<CalendarDateCellProps> = (props) => {
  const { mode, handleChangeDate } = useCalendarContext();

  const { date } = props;

  const handleClickDate = () => {
    handleChangeDate(date);
  };
  return (
    <Primitive.div onClick={handleClickDate}>
      {date.toISOString()}
    </Primitive.div>
  );
};
export default DateCell;
