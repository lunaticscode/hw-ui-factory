import { useMemo } from "react";
import { CalendarMode, useCalendarContext } from "../Root";

const getDatesByMode = (selectedDate: Date, mode: CalendarMode) => {
  if (mode === "days") {
    return [];
  }
  if (mode === "months") {
    return [];
  }
};

const useCalendarDate = () => {
  const { selectedDate = new Date(), mode = "days" } = useCalendarContext();

  const [currentYear, currentMonth] = useMemo(
    () => [selectedDate.getFullYear(), selectedDate.getMonth()],
    [selectedDate]
  );
  const [monthlyFirstDate, monthlyLastDate] = useMemo(
    () => [
      new Date(currentYear, currentMonth, 1),
      new Date(currentYear, currentMonth + 1, 0),
    ],
    [currentYear, currentMonth]
  );

  const monthlyFirstDay = monthlyFirstDate.getDay();
  const firstDate = new Date(
    monthlyFirstDate.getTime() - 3600 * 24 * 1000 * monthlyFirstDay
  );

  const monthlyLastDay = monthlyLastDate.getDay();
  const lastDate = new Date(
    monthlyLastDate.getTime() + 3600 * 24 * 1000 * (6 - monthlyLastDay)
  );

  const length = useMemo(
    () => (lastDate.getTime() - firstDate.getTime()) / (3600 * 1000 * 24) + 1,
    [lastDate, firstDate]
  );

  const dates = useMemo(
    () =>
      Array.from(
        { length },
        (_, index) => new Date(firstDate.getTime() + 3600 * 1000 * 24 * index)
      ),
    [firstDate, length]
  );
  return { dates };
};
export default useCalendarDate;
