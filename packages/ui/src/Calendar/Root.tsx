import { useControlled } from "@repo/libs";
import {
  PropsWithChildren,
  forwardRef,
  createContext,
  useContext,
  useImperativeHandle,
  useState,
} from "react";

export type CalendarMode = "days" | "months" | "years";
export interface CalendarRootProps extends PropsWithChildren {
  mode?: CalendarMode;
  value?: Date;
  defaultValue?: Date;
  onValueChange?: (date: Date) => void;
}

export interface CalendarContextProps extends Pick<CalendarRootProps, "mode"> {
  handleChangeMode: (mode: CalendarMode) => void;
  selectedDate?: Date;
  handleChangeDate: (date: Date) => void;
  handleChangeNavigator: (direction: -1 | 1) => void;
}

export interface CalendarHandlerRef {
  prev?: () => void;
  next?: () => void;
  changeMode?: (mode: CalendarMode) => void;
}

const CalendarContext = createContext<CalendarContextProps>({
  mode: "days",
  handleChangeMode: () => {},
  selectedDate: new Date(),
  handleChangeDate: () => {},
  handleChangeNavigator: () => {},
});

export const useCalendarContext = () => {
  const context = useContext(CalendarContext);
  if (!context) {
    throw Error("(!) Calendar 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};

const getChangedDate = (date: Date, direction: -1 | 1, mode: CalendarMode) => {
  // Move one month
  if (mode === "days") {
    return new Date(
      new Date(date.getFullYear(), date.getMonth() + direction, date.getDate())
    );
  }
  // Move one year
  if (mode === "months") {
    return new Date(
      new Date(date.getFullYear() + direction, date.getMonth(), date.getDate())
    );
  }
};

const CalendarRoot = forwardRef<CalendarHandlerRef, CalendarRootProps>(
  (props, ref) => {
    const {
      children,
      value: valueProp,
      defaultValue: defaultValueProp,
      onValueChange,
    } = props;

    const [mode, setMode] = useState<CalendarMode>("days");
    const { value, setValue } = useControlled(valueProp, defaultValueProp);

    const handleChangeDate = (date: Date) => {
      setValue(date);
      onValueChange?.(date);
    };

    const handleChangeMode = (mode: CalendarMode) => {
      setMode(mode);
    };

    const handleChangeNavigator = (direction: 1 | -1) => {
      setValue(getChangedDate(value || new Date(), direction, mode));
    };

    useImperativeHandle(ref, () => ({
      prev: () => handleChangeNavigator(-1),
      next: () => handleChangeNavigator(1),
      changeMode: handleChangeMode,
    }));

    const contextValue: CalendarContextProps = {
      mode,
      handleChangeMode,
      selectedDate: value,
      handleChangeDate,
      handleChangeNavigator,
    };

    return (
      <CalendarContext.Provider value={contextValue}>
        {children}
      </CalendarContext.Provider>
    );
  }
);

export default CalendarRoot;
