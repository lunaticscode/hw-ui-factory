import Root from "./Root";
import Body from "./Body";
import Current from "./Current";
import Navigator from "./Navigator";

const Calendar = {
  Root,
  Body,
  Current,
  Navigator,
};

export default Calendar;
