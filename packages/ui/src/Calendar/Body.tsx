import { FC, ReactNode } from "react";
import useCalendarDate from "./hooks/useCalendarDate";
import DateCell from "./internal/DateCell";

export interface CalendarBodyProps {
  children?: (dates: Date[]) => ReactNode;
}
const CalendarBody: FC<CalendarBodyProps> = (props) => {
  const { dates } = useCalendarDate();
  const { children } = props;
  if (children) {
    return children?.(dates);
  }
  return (
    <>
      {dates.map((date, index) => (
        <DateCell key={`date-cell-${index}`} date={date} />
      ))}
    </>
  );
};
export default CalendarBody;
