import { FC, ReactNode } from "react";
import { useCalendarContext } from "./Root";
import Primitive from "../Primitve";

export interface CalendarNavigatorProps {
  children?: (handlePrev: () => void, handleNext: () => void) => ReactNode;
}

const CalendarNavigator: FC<CalendarNavigatorProps> = (props) => {
  const { children } = props;
  const { handleChangeNavigator } = useCalendarContext();
  const handlePrev = () => {
    handleChangeNavigator(-1);
  };
  const handleNext = () => {
    handleChangeNavigator(1);
  };
  if (children) {
    return <>{children(handlePrev, handleNext)}</>;
  }
  return (
    <Primitive.div>
      <Primitive.button onClick={handlePrev}>prev</Primitive.button>
      <Primitive.button onClick={handleNext}>next</Primitive.button>
    </Primitive.div>
  );
};
export default CalendarNavigator;
