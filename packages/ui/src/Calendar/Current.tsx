import Primitive from "../Primitve";
import { CalendarMode, useCalendarContext } from "./Root";

const getCurrentDateByMode = (date: Date, mode: CalendarMode) => {
  const mapModeToDate: { [key in CalendarMode]: Date } = {
    days: new Date(date.getFullYear(), date.getMonth(), date.getDate()),
    months: new Date(date.getFullYear(), date.getMonth()),
    years: new Date(date.getFullYear()),
  };
  return mapModeToDate[mode];
};

const CalendarCurrent = () => {
  const { selectedDate = new Date(), mode = "days" } = useCalendarContext();
  return (
    <Primitive.div>
      {getCurrentDateByMode(selectedDate, mode).toDateString()}
    </Primitive.div>
  );
};
export default CalendarCurrent;
