import Provider from "./Provider";
import Page from "./Page";

const Tracking = {
  Provider,
  Page,
};
export default Tracking;
