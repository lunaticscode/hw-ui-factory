import { PropsWithChildren, FC, useEffect, useId, useRef } from "react";
import { useTrackingContext } from "./Provider";

export interface TrackingPageProps extends PropsWithChildren {
  screenName?: string;
}

const Page: FC<TrackingPageProps> = (props) => {
  const screenStartTime = useRef<number>(0);
  const { children, screenName = `tracking_screen_name_${useId()}` } = props;
  const { logger, appId } = useTrackingContext();
  const mounted = () => {
    if (typeof window !== undefined) {
      console.log("asdasd");
      const screenUrl = window.location.pathname || "";
      const startTime = new Date().getTime();
      screenStartTime.current = startTime;

      logger({
        type: "page",
        screenName,
        screenUrl,
        timestamp: startTime,
        appId,
      });
    }
  };
  const unMounted = () => {
    const endTime = new Date().getTime();
    const duration = endTime - screenStartTime.current;
    const screenUrl = window.location.pathname || "";
    logger({
      type: "page",
      screenName,
      screenUrl,
      timestamp: endTime,
      duration,
      appId,
    });
  };
  useEffect(() => {
    mounted();
    window.addEventListener("beforeunload", unMounted);
    return () => {
      window.removeEventListener("beforeunload", unMounted);
      unMounted();
    };
  }, []);
  return <>{children}</>;
};
export default Page;
