import { FC, PropsWithChildren, createContext, useContext } from "react";
const DEFAULT_APP_ID = "hw-rui-tracking-app";

interface BaseEvent {
  appId?: string;
  timestamp?: number;
}

export interface PageEvent extends BaseEvent {
  type: "page";
  screenName?: string;
  screenUrl?: string;
  duration?: number;
}

export interface ClickEvent extends BaseEvent {
  type: "click";
  elementName: string;
  elementId: string;
}

type TrackingType = "page" | "click";
type LoggerEvent = PageEvent | ClickEvent;
type EventType<T extends TrackingType> = T extends "page"
  ? PageEvent
  : ClickEvent;
type LoggerFunc = <T extends LoggerEvent["type"]>(data: EventType<T>) => void;

export interface TrackingProviderProps extends PropsWithChildren {
  appId: string;
}

interface TrackingProviderContextProps {
  appId: string;
  logger: LoggerFunc;
}

const TrackingContext = createContext<TrackingProviderContextProps>({
  appId: DEFAULT_APP_ID,
  logger: () => {},
});
export const useTrackingContext = () => {
  const context = useContext(TrackingContext);
  if (!context) {
    throw Error("(!) TrackingProvider 컨텍스트를 호출할 수 없는 범위입니다.");
  }
  return context;
};
const Provider: FC<TrackingProviderProps> = ({
  children,
  appId = DEFAULT_APP_ID,
}) => {
  const logger: LoggerFunc = (data) => {
    console.log(data);
  };
  const contextValue: TrackingProviderContextProps = {
    appId,
    logger,
  };
  return (
    <TrackingContext.Provider value={contextValue}>
      {children}
    </TrackingContext.Provider>
  );
};
export default Provider;
