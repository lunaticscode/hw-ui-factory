import Root from "./Root";
import Trigger from "./Trigger";
import "../styles/common/index.scss";
const Masthead = {
  Root,
  Trigger,
};

export default Masthead;
