import { PropsWithChildren, forwardRef } from "react";
import { useMastheadContext } from "./Root";

export interface MastheadTriggerProps extends PropsWithChildren {}

const Trigger = forwardRef<HTMLButtonElement, MastheadTriggerProps>(
  (props, ref) => {
    const { children } = props;
    const { active, handleActive } = useMastheadContext();

    const handleClickTrigger = () => {
      handleActive?.(!active);
    };

    return (
      <button
        ref={ref}
        className={"test-button-wrapper"}
        data-active={active}
        onClick={handleClickTrigger}
      >
        {children}
      </button>
    );
  }
);

export default Trigger;
