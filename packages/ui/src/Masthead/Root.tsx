import {
  FC,
  PropsWithChildren,
  createContext,
  useContext,
  useState,
} from "react";

interface MastheadContextProps {
  active?: boolean;
  handleActive?: (active: boolean) => void;
}

const MastheadContext = createContext<MastheadContextProps>({
  active: undefined,
  handleActive: () => {},
});

export const useMastheadContext = () => {
  const context = useContext(MastheadContext);
  if (!context) {
    throw new Error("(!) MastheadContext를 호출할 수 없는 범위입니다.");
  }
  return context;
};

export interface MastheadRootProps extends PropsWithChildren {
  defaultActive?: boolean;
  onActive?: (active: boolean) => void;
}

const Root: FC<MastheadRootProps> = (props) => {
  const { children, defaultActive, onActive } = props;
  const [active, setActive] =
    useState<MastheadContextProps["active"]>(defaultActive);

  const handleActive = (_active: boolean) => {
    setActive(_active);
    onActive?.(_active);
  };
  const contextValue = {
    active,
    handleActive,
  };
  return (
    <MastheadContext.Provider value={contextValue}>
      {children}
    </MastheadContext.Provider>
  );
};

export default Root;
