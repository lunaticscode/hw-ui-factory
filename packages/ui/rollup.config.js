const fs = require("fs");
const path = require("path");
const packageJson = require("./package.json");
const typescript = require("rollup-plugin-typescript2");
const postcss = require("rollup-plugin-postcss");
const commonjs = require("@rollup/plugin-commonjs");
const postcssPrefixer = require("postcss-prefixer");
const peerDpesExternals = require("rollup-plugin-peer-deps-external");
const resolve = require("@rollup/plugin-node-resolve");
const terser = require("@rollup/plugin-terser");

const packageName = "hw-rui";
const extensions = [".js", ".jsx", ".ts", ".tsx", ".scss"];

const getPacakgeJsonData = () => {
  const { react: reactVersion } = packageJson.devDependencies;
  return {
    name: packageName,
    main: "./index.js",
    module: "./index.esm.js",
    types: "./types/index.d.ts",
    peerDependencies: {
      react: reactVersion,
      "react-dom": reactVersion,
    },
  };
};

/** @type  {() => import('rollup').PluginHooks} */
const onBuildEnd = () => {
  return {
    buildEnd: (error) => {
      if (error) {
        console.error("(!) Error ::: ", error);
      } else {
        fs.writeFileSync(
          path.resolve(__dirname, "build", "package.json"),
          JSON.stringify(getPacakgeJsonData())
        );
      }
    },
  };
};

/** @type {import('rollup').RollupOptions} */
const options = {
  input: "src/index.ts",
  output: [
    {
      file: packageJson.main,
      format: "cjs",
      sourcemap: false,
    },
    {
      file: packageJson.module,
      format: "esm",
      sourcemap: false,
    },
  ],

  plugins: [
    resolve({ extensions }),
    peerDpesExternals(),
    terser(),
    commonjs({ include: /node_modules/, sourceMap: false }),
    typescript({ useTsconfigDeclarationDir: true, clean: true }),
    postcss({
      extract: true,
      modules: true,
      sourceMap: true,
      use: ["sass"],
      plugins: [
        postcssPrefixer({
          prefix: `hw-rui__`,
        }),
      ],
    }),
    onBuildEnd(),
  ],
  external: ["react", "react-dom"],
};

module.exports = options;
