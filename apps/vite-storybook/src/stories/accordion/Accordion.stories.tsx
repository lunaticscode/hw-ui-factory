import type { Meta, StoryObj } from "@storybook/react";
import { Accordion } from "@repo/ui";
import { getWithoutFragmentChildren } from "../utils";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta: Meta<(typeof Accordion)["Root"]> = {
  title: "hw-rui/components/Accordion",
  component: Accordion.Root,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    children: getWithoutFragmentChildren(
      <>
        <Accordion.Item>
          <Accordion.Header>
            <Accordion.Trigger />
          </Accordion.Header>
          <Accordion.Content>Accodion-Content-1</Accordion.Content>
        </Accordion.Item>
      </>
    ),
  },
};
