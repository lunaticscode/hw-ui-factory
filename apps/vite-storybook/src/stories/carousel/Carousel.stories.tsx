import type { Meta, StoryObj } from "@storybook/react";
import { Carousel } from "@repo/ui";
import { ReactElement, ReactNode } from "react";
import { getWithoutFragmentChildren } from "../utils";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta: Meta<(typeof Carousel)["Root"]> = {
  title: "hw-rui/components/Carousel",
  component: Carousel.Root,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    children: getWithoutFragmentChildren(
      <>
        <Carousel.Slider>
          <Carousel.Slide>Carousel-Slide-1</Carousel.Slide>
          <Carousel.Slide>Carousel-Slide-2</Carousel.Slide>
        </Carousel.Slider>
        <Carousel.Trigger />
        <Carousel.Pagination />
      </>
    ),
  },
};
