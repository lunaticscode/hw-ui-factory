import type { Meta, StoryObj } from "@storybook/react";
import { Popover } from "@repo/ui";
import { getWithoutFragmentChildren } from "../utils";

// More on how to set up stories at: https://storybook.js.org/docs/writing-stories#default-export
const meta: Meta<(typeof Popover)["Root"]> = {
  title: "hw-rui/components/Popover",
  component: Popover.Root,
  tags: ["autodocs"],
};

export default meta;
type Story = StoryObj<typeof meta>;

export const Basic: Story = {
  args: {
    children: getWithoutFragmentChildren(
      <>
        <Popover.Trigger />
        <Popover.Portal>
          <Popover.Content>
            PopoverContent
            <Popover.Close />
          </Popover.Content>
        </Popover.Portal>
      </>
    ),
  },
};
