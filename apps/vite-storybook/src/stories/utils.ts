import { ReactElement } from "react";

export const getWithoutFragmentChildren = (children: ReactElement) => {
  return children.props.children;
};
