import { PluginOption, defineConfig } from "vite";
import react from "@vitejs/plugin-react";

const CLICK_EVENTS = ["onClick"];

const loggingPlugin = ({}): PluginOption => {
  return {
    name: "",
  };
};

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [react()],
});
