import { useNavigate } from "react-router-dom";
const navMenus = [
  {
    path: "/",
    title: "intro",
  },
  {
    path: "/profile",
    title: "profile",
  },
];
const Nav = () => {
  const navigate = useNavigate();
  return navMenus.map(({ path, title }) => (
    <button key={`nav-key-${path}`} onClick={() => navigate(path)}>
      {title}
    </button>
  ));
};
export default Nav;
