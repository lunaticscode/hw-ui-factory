import { Outlet } from "react-router-dom";
import Nav from "./Nav.tsx";
import { Tracking } from "@repo/ui";
const Layout = () => {
  return (
    <>
      <Nav />
      <Tracking.Provider>
        <Outlet />
      </Tracking.Provider>
    </>
  );
};
export default Layout;
