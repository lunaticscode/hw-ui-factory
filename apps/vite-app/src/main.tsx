import ReactDOM from "react-dom/client";
// import App from "./App.tsx";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Layout from "./components/Layout.tsx";
import IntroPage from "./pages/Intro.tsx";
import ProfilePage from "./pages/Profile.tsx";
import App from "./App.tsx";
const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      { path: "/", element: <IntroPage /> },
      { path: "/main", element: <App /> },
      { path: "/profile", element: <ProfilePage /> },
    ],
  },
  { path: "*", element: <h2>NotFound 404</h2> },
]);

ReactDOM.createRoot(document.getElementById("root")!).render(
  <RouterProvider router={router} />
);
