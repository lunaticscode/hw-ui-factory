import { CSSProperties, useRef, useState } from "react";
import {
  Accordion,
  Carousel,
  Dialog,
  Dropdown,
  Pagination,
  Popover,
  Tabs,
  Tracking,
} from "@repo/ui";

const speratorStyle: CSSProperties = {
  backgroundColor: "gray",
  width: "500px",
  height: "10px",
  marginTop: "20px",
};
function App() {
  const handleChangeValue = (value?: string[]) => {
    console.log(value);
  };

  const popoverContentRef = useRef<HTMLDivElement>(null);
  const [pageState, setPageState] = useState<number>(1);
  const handlePageChange = (page: number) => {
    setPageState(page);
  };
  const [tabValue, setTabValue] = useState<string>();
  const handleChangeTab = (value: string) => {
    setTabValue(value);
  };
  return (
    <Tracking.Provider appId="hw-rui-test">
      <Tracking.Page screenName="vite-app-test">
        <div style={speratorStyle} />
        <h2>Accordion</h2>
        <Accordion.Root onValueChange={handleChangeValue}>
          <Accordion.Item>
            <Accordion.Header>
              <Accordion.Trigger />
            </Accordion.Header>
            <Accordion.Content>AC-1</Accordion.Content>
          </Accordion.Item>
          <Accordion.Item>
            <Accordion.Header>
              <Accordion.Trigger />
            </Accordion.Header>
            <Accordion.Content>AC-2</Accordion.Content>
          </Accordion.Item>
        </Accordion.Root>
        <div style={speratorStyle} />
        <h2>Carousel</h2>
        <Carousel.Root>
          <Carousel.Slider>
            <Carousel.Slide>Slide-1</Carousel.Slide>
            <Carousel.Slide>Slide-2</Carousel.Slide>
          </Carousel.Slider>
          <Carousel.Trigger>
            {(prev, next) => (
              <>
                <button onClick={prev}>prev-button</button>
                <button onClick={next}>next-button</button>
              </>
            )}
          </Carousel.Trigger>
          <Carousel.Pagination>
            {(pages) =>
              pages.map((index) => (
                <span key={`pagination-button-${index}`}>{index}</span>
              ))
            }
          </Carousel.Pagination>
        </Carousel.Root>
        <div style={speratorStyle} />
        <h2>Popover</h2>
        <Popover.Root>
          <Popover.Trigger />
          <Popover.Portal anchorRef={popoverContentRef}></Popover.Portal>
        </Popover.Root>
        <div ref={popoverContentRef}></div>
        <br />
        <div style={speratorStyle} />
        <h2>Dropdown</h2>
        <Dropdown.Root>
          <Dropdown.Trigger>
            {(handleClick) => <button onClick={handleClick}>trigger</button>}
          </Dropdown.Trigger>
          <Dropdown.Content>asd</Dropdown.Content>
        </Dropdown.Root>
        <br />
        <div style={speratorStyle} />
        <h2>Tabs</h2>
        <Tabs.Root onValueChange={handleChangeTab} value={tabValue}>
          <Tabs.TabList>
            <Tabs.Tab value={"1"}>Tab-1</Tabs.Tab>
            <Tabs.Tab value={"2"}>Tab-2</Tabs.Tab>
            <Tabs.Tab value={"3"}>Tab-3</Tabs.Tab>
          </Tabs.TabList>
          <Tabs.Content value={"1"}>TabContent-1</Tabs.Content>
          <Tabs.Content value={"2"}>TabContent-2</Tabs.Content>
          <Tabs.Content value={"3"}>TabContent-3</Tabs.Content>
        </Tabs.Root>

        <Tabs.Root defaultValue="2">
          <Tabs.TabList>
            <Tabs.Tab value={"1"}>Tab-1</Tabs.Tab>
            <Tabs.Tab value={"2"}>Tab-2</Tabs.Tab>
            <Tabs.Tab value={"3"}>Tab-3</Tabs.Tab>
          </Tabs.TabList>
          <Tabs.Content value={"1"}>TabContent-1</Tabs.Content>
          <Tabs.Content value={"2"}>TabContent-2</Tabs.Content>
          <Tabs.Content value={"3"}>TabContent-3</Tabs.Content>
        </Tabs.Root>
        <br />
        <br />
        <br />
        <h2>Pagination</h2>
        <Pagination.Root itemLength={235} defaultValue={1}>
          <Pagination.PageButtons />
          <Pagination.Navigator />
        </Pagination.Root>

        {/* <Pagination.Root
          itemLength={235}
          value={pageState}
          onPageChange={handlePageChange}
        >
          <Pagination.PageButtons />
          <Pagination.Navigator />
        </Pagination.Root> */}

        <br />
        <br />
        <br />
        <Pagination.Root
          itemLength={235}
          value={pageState}
          onPageChange={handlePageChange}
        >
          <Pagination.PageButtons>
            {(pages, currentPage, handleClickPage) =>
              pages?.map((page) => (
                <span
                  onClick={() => handleClickPage(page)}
                  style={{
                    fontWeight: currentPage === page ? "bold" : "initial",
                  }}
                  key={`page-button-${page}`}
                >
                  {page}
                </span>
              ))
            }
          </Pagination.PageButtons>

          <Pagination.Navigator>
            {(prev, next) => (
              <div>
                <button onClick={prev}>이전</button>
                <button onClick={next}>다음</button>
              </div>
            )}
          </Pagination.Navigator>
        </Pagination.Root>
        <br />
        <br />
        <br />
        <h2>Dialog</h2>
        <Dialog.Root>
          <Dialog.Trigger></Dialog.Trigger>
        </Dialog.Root>
      </Tracking.Page>
    </Tracking.Provider>
  );
}

export default App;
