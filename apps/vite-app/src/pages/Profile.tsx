import { Dropdown, ScrollArea, Tracking } from "@repo/ui";
const dummyTextData = Array.from(
  { length: 30 },
  (_, index) => `dummy-text-${index}`
);
const ProfilePage = () => {
  return (
    <>
      <Tracking.Page>
        <h2>UI TEST</h2>
        <ScrollArea.Root>
          <ScrollArea.Viewport>
            {dummyTextData.map((text, index) => (
              <div key={`dummy-text-${index}`}>{text}</div>
            ))}
          </ScrollArea.Viewport>
          <ScrollArea.ScrollBar>
            <ScrollArea.Thumb />
          </ScrollArea.ScrollBar>
        </ScrollArea.Root>

        <Dropdown.Root>
          <Dropdown.Trigger>asd</Dropdown.Trigger>
          <Dropdown.Content>
            <ScrollArea.Root>
              <ScrollArea.Viewport width={"200px"}>
                {Array.from({ length: 100 }, (_, index) => (
                  <Dropdown.Item
                    key={`Dropdown-item-key${index}`}
                  >{`DropdownItem-${index + 1}`}</Dropdown.Item>
                ))}
              </ScrollArea.Viewport>
              <ScrollArea.ScrollBar>
                <ScrollArea.Thumb />
              </ScrollArea.ScrollBar>
            </ScrollArea.Root>
          </Dropdown.Content>
        </Dropdown.Root>
      </Tracking.Page>
    </>
  );
};
export default ProfilePage;
