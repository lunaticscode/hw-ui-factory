import { Calendar, Popover, Tracking } from "@repo/ui";
import { useRef } from "react";

const IntroPage = () => {
  const triggerRef = useRef<HTMLButtonElement>(null);
  return (
    <Tracking.Page>
      <h2>This is IntroPage</h2>
      <Popover.Root>
        <Popover.Trigger ref={triggerRef}>Trigger</Popover.Trigger>
        <Popover.Portal anchorRef={triggerRef}>
          <Popover.Content>asd</Popover.Content>
        </Popover.Portal>
      </Popover.Root>
      <Calendar.Root>
        <Calendar.Current />
        <Calendar.Navigator />
        <Calendar.Body />
      </Calendar.Root>
    </Tracking.Page>
  );
};
export default IntroPage;
