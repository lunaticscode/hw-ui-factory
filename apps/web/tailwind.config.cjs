/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["app/**/*.{ts,tsx}", "components/**/*.{ts,tsx}"],
  theme: {
    colors: {
      slate: "#64748B",
      blue: "#1fb6ff",
      purple: "#7e5bef",
      pink: "#ff49db",
      orange: "#ff7849",
      yellow: "#ffc82c",
      green: "#22C55E",
      "gray-dark": "#273444",
      gray: "#6B7280",
      "gray-light": "#d3dce6",
    },
    extend: {},
  },
  plugins: [],
};
