import DocsSidebar, { SidebarMenu } from "@/components/DocsSidebar";
import type { Metadata } from "next";

const docsItems: SidebarMenu[] = [
  {
    label: "Accordion",
    href: "/accordion",
  },
  {
    label: "Calendar",
    href: "/calendar",
  },

  {
    label: "Carousel",
    href: "/carousel",
  },
  {
    label: "Dialog",
    href: "/dialog",
  },
  {
    label: "Dropdown",
    href: "/dropdown",
  },
  {
    label: "Pagination",
    href: "/pagination",
  },
  {
    label: "Popover",
    href: "/popover",
  },
  {
    label: "ScrollArea",
    href: "/scrollarea",
  },
  {
    label: "Tabs",
    href: "/tabs",
  },
];

export const metadata: Metadata = {
  title: "Docs - hwuifactory",
  description: "HW-RUI Docs",
};

export default function DocsLayout({
  children,
}: {
  children: React.ReactNode;
}): JSX.Element {
  return (
    <div className={"grid grid-cols-[150px_auto]"}>
      <DocsSidebar items={docsItems} />
      <div>{children}</div>
    </div>
  );
}
