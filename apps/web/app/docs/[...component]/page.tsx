import PageBreadCrumb from "@/components/PageBreadCrumb";
import { revalidatePath } from "next/cache";
import { useRouter } from "next/navigation";
interface DocsPageProps {
  params: {
    component: string[];
  };
}
const prefixBreadcrumb = "Docs";

const getPageBreadcrumbMenus = async ({ params }: DocsPageProps) => {
  console.log(params);
  const components = params.component || [];
  const pascalCasedComponents = components.map((comp) =>
    comp[0] ? comp[0].toUpperCase() + comp.slice(1, comp.length) : ""
  );
  return [prefixBreadcrumb, ...pascalCasedComponents];
};

const DocsPage = async ({ params }: DocsPageProps) => {
  const breadcrumbMenus = await getPageBreadcrumbMenus({ params });
  console.log(breadcrumbMenus);

  return (
    <>
      <PageBreadCrumb crumbs={breadcrumbMenus} />
    </>
  );
};
export default DocsPage;
