"use client";
import Link from "next/link";
import { usePathname } from "next/navigation";
import { FC, ReactNode, useEffect, useState } from "react";

export type SidebarMenu = {
  href?: string;
  disabled?: boolean;
  icon?: ReactNode;
  label?: string;
};

interface DocsSidebarProps {
  items?: SidebarMenu[];
}

const DocsSidebar: FC<DocsSidebarProps> = ({ items }) => {
  const [currentPath, setCurrentPath] = useState<SidebarMenu["href"]>();
  const pathname = usePathname();

  useEffect(() => {
    setCurrentPath(pathname.split("/docs")[1] || "");
  }, [pathname]);

  return (
    <aside className={"w-fit h-[100vh]"}>
      {items?.map(({ label, href }) => (
        <div key={`docs-sidebar-link-${label}`}>
          <Link href={`/docs/${href}` || ""}>
            <span className={currentPath === href ? "font-bold" : ""}>
              {label}
            </span>
          </Link>
        </div>
      ))}
    </aside>
  );
};
export default DocsSidebar;
