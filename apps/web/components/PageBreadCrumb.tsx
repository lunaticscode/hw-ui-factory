import { FC, Fragment } from "react";
import { ChevronRight } from "./icons";

interface PageBreadCrumbProps {
  crumbs: string[];
}
const PageBreadCrumb: FC<PageBreadCrumbProps> = (props) => {
  const { crumbs } = props;
  const isLastCrumbIndex = crumbs.length - 1;
  return (
    <div>
      {crumbs.map((crumb, index, arr) => (
        <Fragment key={`page-${crumb}`}>
          <div
            className={
              isLastCrumbIndex === index ? "inline font-bold" : "inline"
            }
          >
            {crumb}
          </div>
          {isLastCrumbIndex !== index && (
            <ChevronRight
              className={"w-[15px]"}
              stroke="gray"
              strokeWidth={"2px"}
            />
          )}
        </Fragment>
      ))}
    </div>
  );
};
export default PageBreadCrumb;
