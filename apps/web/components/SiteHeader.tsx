import Link from "next/link";

const navItems = [
  { label: "Home", href: "/" },
  { label: "Docs", href: "/docs" },
];
const SiteHeader = () => {
  return (
    <header
      className={"sticky top-0 z-100 w-full border-b-[1px] border-gray-light"}
    >
      <div className="container flex h-14 max-w-screen-2xl items-center">
        {navItems.map(({ label, href }) => (
          <div key={`header-nav-${label}`} className={"ml-[10px]"}>
            <Link href={href}>{label}</Link>
          </div>
        ))}
      </div>
    </header>
  );
};
export default SiteHeader;
