import { FC, SVGProps } from "react";

export interface IconProps extends FC<SVGProps<SVGSVGElement>> {}
