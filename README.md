`Notion` [HW-UI-FACTORY](https://triangular-nest-0f1.notion.site/HW-UI-FACTORY-d4f8fae0d4bb4661831d905551fb06b9?pvs=4)

### `UI Package(@repo/ui)`

## **Accordion**

### Source

[packages/ui/src/Accordion · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Accordion?ref_type=heads)

### **Children**

- Root
- Item
- Header
- Trigger
- Content

### Example

```jsx
<Accordion.Root>
  <Accordion.Item>
    <Accordion.Header>
      <Accordion.Trigger />
    </Accordion.Header>
    <Accordion.Content>AC-1</Accordion.Content>
  </Accordion.Item>

  <Accordion.Item>
    <Accordion.Header>
      <Accordion.Trigger />
    </Accordion.Header>
    <Accordion.Content>AC-2</Accordion.Content>
  </Accordion.Item>
</Accordion.Root>
```

## **Carousel**

### Source

[packages/ui/src/Carousel · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Carousel?ref_type=heads)

### Children

- Root
- Slider
- Slide
- Trigger
- Pagination

### Example

```jsx
<Carousel.Root>
  <Carousel.Slider>
    <Carousel.Slide>Slide-1</Carousel.Slide>
    <Carousel.Slide>Slide-2</Carousel.Slide>
  </Carousel.Slider>
  <Carousel.Trigger />
  <Carousel.Pagination />
</Carousel.Root>;

{
  /** Custom Trigger */
}
<Carousel.Root>
  <Carousel.Slider>
    <Carousel.Slide>Slide-1</Carousel.Slide>
    <Carousel.Slide>Slide-2</Carousel.Slide>
  </Carousel.Slider>
  <Carousel.Trigger>
    {(prev, next) => (
      <>
        <button onClick={handleClickPrev}>prev-button</button>
        <button onClick={handleClickNext}>next-button</button>
      </>
    )}
  </Carousel.Trigger>
  <Carousel.Pagination>
    {(pages) =>
      pages.map((index) => (
        <span key={`pagination-button-${index}`}>{index}</span>
      ))
    }
  </Carousel.Pagination>
</Carousel.Root>;
```

## **Calendar**

### Source

[packages/ui/src/Calendar · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Calendar?ref_type=heads)

### Children

- Root
- Current
- Navigator
- Body

### Example

```jsx
{
  /* Uncontrolled */
}
<Calendar.Root defaultValue={new Date()}>
  <Calendar.Current />
  <Calendar.Navigator />
  <Calendar.Body />
</Calendar.Root>;

{
  /* Controlled */
}
<Calendar.Root onChange={handleChangeDate} value={date}>
  <Calendar.Current />
  <Calendar.Navigator />
  <Calendar.Body />
</Calendar.Root>;

{
  /* Custom Navigator, Body(DateCell) */
}
<Calendar.Root>
  <Calendar.Navigator>
    {(prev, next) => (
      <>
        <button onClick={prev}>custom-prev</button>
        <button onClick={next}>custom-next</button>
      </>
    )}
  </Calendar.Navigator>
  <Calendar.Body>
    {(dates) => dates.map((date) => <div>{date.toISOString()}</div>)}
  </Calendar.Body>
</Calendar.Root>;
```

## **Dropdown**

### Source

[packages/ui/src/Dropdown · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Dropdown?ref_type=heads)

### **Children**

- Root
- Trigger
- Content
- Item

### **Example**

```jsx
{
  /* Basic */
}
<Dropdown.Root>
  <Dropdown.Trigger>Dropdown-1</Dropdown.Trigger>
  <Dropdown.Content>
    <Dropdown.Item>Item-1</Dropdown.Item>
    <Dropdown.Item>Item-2</Dropdown.Item>
    <Dropdown.Item>Item-3</Dropdown.Item>
  </Dropdown.Content>
</Dropdown.Root>;

{
  /* with ScrollArea */
}
<Dropdown.Root>
  <Dropdown.Trigger>Dropdown-2</Dropdown.Trigger>
  <Dropdown.Content>
    <ScrollArea.Root>
      <ScrollArea.Viewport>
        {Array.from({ length: 100 }, (_, index) => (
          <Dropdown.Item
            key={`Dropdown-item-key${index}`}
          >{`DropdownItem-${index + 1}`}</Dropdown.Item>
        ))}
      </ScrollArea.Viewport>
      <ScrollArea.ScrollBar>
        <ScrollArea.Thumb />
      </ScrollArea.ScrollBar>
    </ScrollArea.Root>
  </Dropdown.Content>
</Dropdown.Root>;
```

## **Pagination**

### Source

[packages/ui/src/Paginator · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Paginator?ref_type=heads)

### Children

- Root
- PageButtons
- Navigator

### Example

```jsx
{
  /* Uncontrolled */
}
<Pagination.Root itemLength={235} defaultValue={1}>
  <Pagination.PageButtons />
  <Pagination.Navigator />
</Pagination.Root>;

{
  /* Controlled */
}
<Pagination.Root itemLength={235} value={page} onPageChange={handlePageChange}>
  <Pagination.PageButtons />
  <Pagination.Navigator />
</Pagination.Root>;

{
  /* Custom PageButton, Navigator */
}
<Pagination.Root
  itemLength={235}
  value={pageState}
  onPageChange={handlePageChange}
>
  <Pagination.PageButtons>
    {(pages, currentPage, handleClickPage) =>
      pages?.map((page) => (
        <span
          onClick={() => handleClickPage(page)}
          style={{ fontWeight: currentPage === page ? "bold" : "initial" }}
          key={`page-button-${page}`}
        >
          {page}
        </span>
      ))
    }
  </Pagination.PageButtons>

  <Pagination.Navigator>
    {(prev, next) => (
      <div>
        <button onClick={prev}>이전</button>
        <button onClick={next}>다음</button>
      </div>
    )}
  </Pagination.Navigator>
</Pagination.Root>;
```

## **Popover**

### Source

[packages/ui/src/Popover · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Popover?ref_type=heads)

### Children

- Root
- Trigger
- Portal
- Content
- Close

### Example

```jsx
{
  /* PortalContainer: document.body */
}
<Popover.Root>
  <Popover.Trigger>popover-open</Popover.Trigger>
  <Popover.Portal>
    <Popover.Content>Popover Content</Popover.Content>
  </Popover.Portal>
</Popover.Root>;

{
  /* PortalContainer: triggerRef */
}
<Popover.Root>
  <Popover.Trigger ref={triggerRef}>popover-open</Popover.Trigger>
  <Popover.Portal anchorRef={triggerRef}>
    <Popover.Content>Popover Content</Popover.Content>
  </Popover.Portal>
</Popover.Root>;
```

## **ScrollArea**

### Source

[packages/ui/src/ScrollArea · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/ScrollArea?ref_type=heads)

### Children

- Root
- Viewport
- ScrollBar
- Thumb

### Example

```jsx
<ScrollArea.Root>
  <ScrollArea.Viewport>
    {dummyTextData.map((text, index) => (
      <div key={`dummy-text-${index}`}>{text}</div>
    ))}
  </ScrollArea.Viewport>
  <ScrollArea.ScrollBar>
    <ScrollArea.Thumb />
  </ScrollArea.ScrollBar>
</ScrollArea.Root>
```

## Tabs

### **Source**

[packages/ui/src/Tabs · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Tabs?ref_type=heads)

### **Children**

- Root
- TabList
- Tab
- Content

### **Example**

```jsx
{
  /* Controlled */
}
<Tabs.Root onValueChange={handleChangeTab} value={tabValue}>
  <Tabs.TabList>
    <Tabs.Tab value={"1"}>Tab-1</Tabs.Tab>
    <Tabs.Tab value={"2"}>Tab-2</Tabs.Tab>
    <Tabs.Tab value={"3"}>Tab-3</Tabs.Tab>
  </Tabs.TabList>
  <Tabs.Content value={"1"}>TabContent-1</Tabs.Content>
  <Tabs.Content value={"2"}>TabContent-2</Tabs.Content>
  <Tabs.Content value={"3"}>TabContent-3</Tabs.Content>
</Tabs.Root>;

{
  /* Uncontrolled */
}
<Tabs.Root defaultValue={TAB_DEFAULT_VALUE}>
  <Tabs.TabList>
    <Tabs.Tab value={"1"}>Tab-1</Tabs.Tab>
    <Tabs.Tab value={"2"}>Tab-2</Tabs.Tab>
    <Tabs.Tab value={"3"}>Tab-3</Tabs.Tab>
  </Tabs.TabList>
  <Tabs.Content value={"1"}>TabContent-1</Tabs.Content>
  <Tabs.Content value={"2"}>TabContent-2</Tabs.Content>
  <Tabs.Content value={"3"}>TabContent-3</Tabs.Content>
</Tabs.Root>;
```

## **Tracking**

### Source

[packages/ui/src/Tracking · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/tree/main/packages/ui/src/Tracking?ref_type=heads)

### Children

- Provider
- Page

### Example

```jsx
<Tracking.Provider appId={APP_ID}>
  <Tracking.Page>
    <PageComponent />
  </Tracking.Page>
</Tracking.Provider>
```

```json
// Mounted
{
 "appId": "hw-rui-tracking-app",
 "type": "page",
 "screenName": "tracking_screen_name_:r0:",
 "screenUrl": "/",
 "timestamp": 1718522327585,
}

// Unmounted
{
  "appId": "hw-rui-tracking-app",
  "type": "page",
  "screenName": "tracking_screen_name_:r0:",
  "screenUrl": "/",
  "timestamp": 1718522385906,
  "duration": 58321,
}
```

##

### `Libs (@repo/libs)`

### **Hooks**

### **useControlled**

### Source

[packages/libs/src/hooks/useControlled.ts · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/blob/main/packages/libs/src/hooks/useControlled.ts)

### **useDebounce**

### Source

[packages/libs/src/hooks/useDebounce.ts · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/blob/main/packages/libs/src/hooks/useDebounce.ts)

### **useDebounceCallback**

### **Source**

[packages/libs/src/hooks/useDebounceCallback.ts · main · HumanWater / hw-ui-factory · GitLab](https://gitlab.com/lunaticscode/hw-ui-factory/-/blob/main/packages/libs/src/hooks/useDebounceCallback.ts)

##

### `Cli (@repo/cli)`
